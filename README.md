- [NL AIC Data sharing: Public services PoC](#nl-aic-data-sharing-public-services-poc)
  - [Technical implementation](#technical-implementation)
  - [Project structure](#project-structure)
  - [Running the demo](#running-the-demo)
    - [Data owners or legal entities provide permission (Rule manager)](#data-owners-or-legal-entities-provide-permission-rule-manager)
    - [Data service consumer requests the risk overview (Municipality front-end)](#data-service-consumer-requests-the-risk-overview-municipality-front-end)
    - [Contract negotiation process sequence diagram](#contract-negotiation-process-sequence-diagram)

# NL AIC Data sharing: Public services PoC
This project contains the documentation and code for a Proof of Concept for the Public services sector, initiated by the Dutch AI Coalition [(NL-AIC)](https://nlaic.com/).
An extensive, less technical desciption of the PoC can be found in the [GAP Analysis, Section 3.1](https://nlaic.com/wp-content/uploads/2021/03/NL-AIC-GAP-Analysis.pdf).

The goal of this PoC is to provide an example implementation of the permission management architecture for lawful grounding and accountability, as described in the GAP-analysis *"From data sharing proofs-of-concept towards operationalization of the system architecture"*.
The PoC builds upon the ‘data-to-analysis’ collaboration model, in which an AI-algorithm provider (CBS) gathers and combines data from different sources (Belastingdienst, CJIB and CAK) and shares the analysis result with a third party, the municipality of Amsterdam.
The PoC demonstrates the required processes for lawful grounding and accountability. 

The PoC includes two main permission management processes:
- *The fulfillment process (configuration)*:
In the published document, the Subject Rule Manager, the Community Rule Manager and the Legal Rule Manager are described as building blocks where one can manage and register data sharing rules in natural language.
In this PoC, a simplified and combined *Rule Manager* is present that manages data sharing rules for individual subjects and legal entities.

- *The data transaction process (usage)*:
In the data transaction process, a Data Service Consumer requests data from a Data Service Provider. 
In this PoC, the Data Service Consumer is the Municipality of Amsterdam, while the Data Service Providers are CJIB, CAK and Belastingdienst.
The CBS is the analysis party and acts as both Data Service Consumer (from CJIB, CAK and Belastingdienst) and Data Service Provider (to Municipality).
Policy enforcement using the Policy Enforcement Framework (PEF) is built into the Data Service Provides, where they verify that a valid contract is negotiated according to data sharing rules set in the configuration phase.

## Technical implementation
The implementatation is based on [Interational Data Spaces (IDS)](https://www.internationaldataspaces.org).
So, some prior knowledge on e.g. what a Data App is, is assumed.
If you have no prior knowledge on IDS, the [Reference Architecture Model](https://www.internationaldataspaces.org/ids-ram-3-0/) is a good starting point.

>*NOTE:* This project is part of a Proof of Concept and therefore does not contain production ready code!
The PoC use case is purely fictional and the intent is to show the general concept, and does not have a direct relation with real processes of public services.

## Project structure
The PoC consists of multiple microservices representing (parts of) entities in the public services sector.
Every service is present as subfolder in this repository:
- [analysis-data-app](./analysis-data-app/README.md) contains the IDS data app representing the CBS.
- [authorization-registry-ui](./authorization-registry-ui/README.md) contains a front-end where authorizations are registered.
- [authorization-registry](./authorization-registry/README.md) contains the code representing the Authorization Registry.
- [consumer-data-app](./consumer-data-app/README.md) contains the code representing the Municipality.
- [consumer-gui](./consumer-gui/README.md) contains the front-end of the Municipality to show the end results.
- [lawful-ground-manager](./lawful-ground-manager/README.md) contains the Lawful Ground manager, responsible for building contracts used for data sharing transactions.
- [provider-data-app](./provider-data-app/README.md) contains the code representing Data Service Provides (Belastingdienst, CAK, CJIB).
- [rule-manager](./rule-manager/README.md) contains the Rule Manager IDS Data App where data subject and legal rules are registered.
- [rule-manager-ui](./rule-manager-ui/README.md) contains the front-end for the Rule Manager.
- [helm-chart](/helm-chart/README.md) contains the Helm chart used for deploying the services to a Kubernetes cluster.

## Running the demo
In accordance with the introduction, the demo needs two steps to perform successful data sharing.
1. Data owners or legal entities provide permission in the Rule manager
2. Data service consumer requests a risk overview


### Data owners or legal entities provide permission (Rule manager)
1. Navigate to the Rule Manager
2. Log in as "urn:gov:legal" using any password.
3. Make sure a rule with *Allow debt analysis* checked exists for each data service provider:
![rules](./img/rules.png)


### Data service consumer requests the risk overview (Municipality front-end)
1. Navigate to the consumer and click Show.
2. The results will show up:
![risk overview](./img/risk-overview.png)
3. Navigate to the *Authorization Registry* and verify there are new registered contracts:
![authorization-registry](./img/authorization-registry.png)


### Contract negotiation process sequence diagram
A sequence diagram of the contract negotiation process with related IDS messages that happens behind the scenes is shown below:

```mermaid
sequenceDiagram
    title: Contract negotiation for risk overview request
    participant M as Municipality
    participant C as CBS
    participant P as Data Service Providers
    participant LGM as Lawful Ground Manager
    participant RM as Rule Manager
    M->>C: ArtifactRequest
    loop Tax Authority, CJIB, CAK
        C->>P: ContractRequest
        P->>LGM: ContractRequest(dataOwners)
        LGM->>RM: permissions := Request(permissions)
        alt permissions accepted
            LGM->>P: ContractOffer(permissions)
            P->>C: ContractOffer(permissions)
            C->>+P: ArtifactRequest()
            P->>P: policyEnforcement()
            P-->>-C: dataSet := ArtifactResponse()
        else permissions rejected
            LGM->>P: ContractRejection()
            P->>C: ContractRejection()
        end
    end
    alt all datasets received
        C->>C: analysis := DebtAnalysis(dataSets)
        C->>M: ArtifactResponse(analysis)
    else any contract rejected
        C->>M: Rejection()
    end
```
