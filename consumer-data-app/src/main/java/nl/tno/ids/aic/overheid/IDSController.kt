package nl.tno.ids.aic.overheid

import nl.tno.ids.base.IDSRestController
import nl.tno.ids.base.MessageHandler
import org.springframework.boot.autoconfigure.EnableAutoConfiguration
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration
import org.springframework.scheduling.annotation.EnableScheduling
import org.springframework.web.bind.annotation.RestController

@EnableScheduling
@RestController
@EnableAutoConfiguration(exclude = [DataSourceAutoConfiguration::class, HibernateJpaAutoConfiguration::class])
open class IDSController(messageHandlers: List<MessageHandler<*>>): IDSRestController() {
  init {
    LOG.info("Starting Consumer message handler.")
    for(messageHandler in messageHandlers) {
      LOG.info("Registering message handler: ${messageHandler::class.java}")
      this.registerMessageHandler(messageHandler)
    }
  }
}