package nl.tno.ids.aic.overheid.outgoing

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.registerKotlinModule
import de.fraunhofer.iais.eis.ArtifactRequestMessageBuilder
import de.fraunhofer.iais.eis.util.Util
import nl.tno.ids.base.IDSRestController
import nl.tno.ids.common.serialization.Config
import nl.tno.ids.common.serialization.DateUtil
import com.fasterxml.jackson.module.kotlin.*
import de.fraunhofer.iais.eis.RejectionMessage
import org.slf4j.LoggerFactory
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import java.net.URI


class DataConsumerConfig {
  var cbsId: String = ""
}

data class Person(var id: Int, var score: Int)

@RestController
@RequestMapping("api")
class DataConsumerController {
  private val _dataAppConfig = Config.dataApp()
  private val _dataConsumerConfig = Config.dataApp().getCustomProperties(DataConsumerConfig::class.java)

  companion object {
    private val LOG = LoggerFactory.getLogger(DataConsumerController::class.java)
  }

  /**
   * Send a ArtifactRequestMessage to CBS, parse it and return it as JSON
   */
  @GetMapping("person-risks")
  fun getHighRiskPersons(): ResponseEntity<List<Person>> {
    LOG.info("Send ArtifactRequestMessage to ${_dataConsumerConfig.cbsId}")
    val response = IDSRestController.sendHTTP(_dataConsumerConfig.cbsId, ArtifactRequestMessageBuilder()
        ._modelVersion_("3.1.0")
        ._issued_(DateUtil.now())
        ._issuerConnector_(URI.create(_dataAppConfig.id))
        ._senderAgent_(URI.create(_dataAppConfig.participant))
        // TODO: Is the recipient the same as the the issuer?
        ._recipientConnector_(Util.asList(URI.create(_dataAppConfig.id)))
        // TODO: participant?
        ._recipientAgent_(Util.asList(URI.create(_dataAppConfig.participant)))
        // TODO: requested artifact
        ._requestedArtifact_(URI.create("data.csv"))
        .build(), "")
    if (response.second.header is RejectionMessage) {
      LOG.info("Artifact Request rejected by ${response.second.header.id}, returning HTTP forbidden")
      return ResponseEntity.status(HttpStatus.FORBIDDEN).build()
    }
    LOG.info("Received ArtifactResponse from ${response.second.header.id}")
    return ResponseEntity.ok(jacksonObjectMapper().readValue(response.second.payload))
  }
}