# Consumer GUI
This repository contains a Vue application that acts as the front-end for the Authorization registry.

>*NOTE:* This project is part of a Proof of Concept and therefore does not contain production ready code!

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

## Configuration
The Consumer GUI needs the following environment variables set to work properly

| Key         | Description                                |
| ----------- | ------------------------------------------ |
| API_BACKEND | Endpoint of the Authorization Registry API |