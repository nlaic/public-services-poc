# Analysis data app
This repository contains a Kotlin application that acts as providing data app for the Belastingdienst, CAK and CJIB.

>*NOTE:* This project is part of a Proof of Concept and therefore does not contain production ready code!

## Building the project
Gradle is used as build tool, build the project by running ``./gradlew build``.

## Configuration
The Data App needs some YAML configuration values placed in ``/ids/config.yaml`` to work properly:

| Key                           | Description                                  |
| ----------------------------- | -------------------------------------------- |
| id                            | URN of an ID                                 |
| participant                   | URN of the participant                       |
| customProperties.resourcePath | Path of the dataset loaded into the data app |
