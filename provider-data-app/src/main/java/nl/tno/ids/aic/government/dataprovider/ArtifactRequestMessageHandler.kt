package nl.tno.ids.aic.government.dataprovider

import de.fraunhofer.iais.eis.ArtifactRequestMessage
import de.fraunhofer.iais.eis.ArtifactResponseMessage
import de.fraunhofer.iais.eis.ArtifactResponseMessageBuilder
import de.fraunhofer.iais.eis.util.Util
import nl.tno.ids.aic.government.DataProviderConfig
import nl.tno.ids.base.MessageHandler
import nl.tno.ids.common.multipart.MultiPart
import nl.tno.ids.common.multipart.MultiPartMessage
import nl.tno.ids.common.serialization.Config
import nl.tno.ids.common.serialization.DateUtil
import org.slf4j.LoggerFactory
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import java.io.File
import java.net.URI

class ArtifactRequestMessageHandler : MessageHandler<ArtifactRequestMessage> {
  private var _dataAppConfig = Config.dataApp()
  private var _dataProviderConfig = _dataAppConfig.getCustomProperties(DataProviderConfig::class.java)

  companion object {
    private val LOG = LoggerFactory.getLogger(ArtifactRequestMessageHandler::class.java)
  }

  /**
   * Read a file defined in resourcePath property and return it in the ArtifactResponse
   */
  override fun handle(header: ArtifactRequestMessage, payload: String?): ResponseEntity<String>? {
    LOG.info("Received ArtifactRequestMessage, sending contents of ${_dataProviderConfig.resourcePath} back as ArtifactResponseMessage")
    val dataSet = File(_dataProviderConfig.resourcePath).readText()
    return MultiPart.toResponseEntity(
      MultiPartMessage.Builder()
        .setPayload(dataSet)
        .setHeader(artifactResponseMessageHeaderBuilder(header.id, header.issuerConnector.toString()))
        .build(), HttpStatus.OK
    )
  }

  private fun artifactResponseMessageHeaderBuilder(
    requestId: URI,
    recipientConnector: String
  ): ArtifactResponseMessage {
    return ArtifactResponseMessageBuilder()
      ._correlationMessage_(requestId)
      // TODO: transfer contract uri. But we don't know the contract Id here
      ._transferContract_(URI.create("todo"))
      ._modelVersion_("3.0.0")
      ._issued_(DateUtil.now())
      ._issuerConnector_(URI.create(Config.dataApp().id))
      ._senderAgent_(URI.create(Config.dataApp().participant))
      ._recipientAgent_(Util.asList(URI.create(Config.dataApp().participant)))
      ._recipientConnector_(Util.asList(URI.create(recipientConnector)))
      .build()
  }
}
