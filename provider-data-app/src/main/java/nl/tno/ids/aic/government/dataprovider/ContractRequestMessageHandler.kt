package nl.tno.ids.aic.government.dataprovider

import de.fraunhofer.iais.eis.*
import de.fraunhofer.iais.eis.util.Util
import nl.tno.ids.aic.government.DataProviderConfig
import krangl.DataFrame
import krangl.readDelim
import krangl.toStrings
import nl.tno.ids.base.IDSRestController
import nl.tno.ids.base.MessageHandler
import nl.tno.ids.common.multipart.MultiPart
import nl.tno.ids.common.multipart.MultiPartMessage
import nl.tno.ids.common.serialization.Config
import nl.tno.ids.common.serialization.DateUtil
import nl.tno.ids.common.serialization.SerializationHelper
import org.slf4j.LoggerFactory
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import java.io.ByteArrayInputStream
import java.io.File
import java.io.InputStream
import java.net.URI

class ContractRequestMessageHandler : MessageHandler<ContractRequestMessage> {
  private var _dataAppConfig = Config.dataApp()
  private var _dataProviderConfig = Config.dataApp().getCustomProperties(DataProviderConfig::class.java)

  companion object {
    private val LOG = LoggerFactory.getLogger(ArtifactRequestMessageHandler::class.java)
  }

  override fun handle(header: ContractRequestMessage?, payload: String?): ResponseEntity<*> {
    LOG.info("Parsing Received contract")
    var receivedContractRequest: ContractRequest =
      SerializationHelper.getInstance().fromJsonLD(payload, ContractRequest::class.java)
    LOG.info("Retrieve list of subject data owners refered to in the dataset")
    val dataSetDataOwners = getDatasetDataOwners()
    var requestPermissions: ArrayList<Permission> = arrayListOf()

    LOG.info("Rebuilding request permission rules with subject data owners")
    // Map data set owner Ids to participants
    val participants =
      ArrayList<Participant>(dataSetDataOwners.map { dataOwner -> ParticipantBuilder(URI.create(dataOwner)).build() })

    for (p in receivedContractRequest.permission) {
      val requestPermission: Permission = PermissionBuilder()
        ._assigner_((participants))
        // Take the consumer from the contractRequest and pass it to provider lawful ground manager
        ._assignee_(ArrayList<Participant>(listOf(ParticipantBuilder(receivedContractRequest.consumer).build())))
        ._constraint_(p.constraint)
        .build()
      requestPermissions.add(requestPermission)
    }

    // Copy the received contract WITH the subject data owners in the data set in the permission fields
    LOG.info("Rebuilding the Contract request with updated permissions")
    receivedContractRequest = ContractRequestBuilder()
      ._contractStart_(receivedContractRequest.contractStart)
      ._consumer_(receivedContractRequest.consumer)
      ._contractAnnex_(receivedContractRequest.contractAnnex)
      ._contractDate_(receivedContractRequest.contractDate)
      ._contractDocument_(receivedContractRequest.contractDocument)
      ._contractEnd_(receivedContractRequest.contractEnd)
      ._obligation_(receivedContractRequest.obligation)
      ._permission_(requestPermissions)
      ._prohibition_(receivedContractRequest.prohibition)
      ._provider_(receivedContractRequest.provider)
      ._refersTo_(receivedContractRequest.refersTo)
      .build()


    val lawfulGroundManager = "urn:ids:connectors:LawfulGroundManager"
    val response = IDSRestController.sendHTTP(
      lawfulGroundManager,
      ContractRequestMessageBuilder()
        ._modelVersion_("3.1.0")
        ._issued_(DateUtil.now())
        ._issuerConnector_(URI.create(_dataAppConfig.id))
        ._senderAgent_(URI.create(_dataAppConfig.id))
        ._recipientConnector_(Util.asList(URI.create(lawfulGroundManager)))
        ._recipientAgent_(Util.asList(URI.create(lawfulGroundManager)))
        .build(),
      SerializationHelper.getInstance().toJsonLD(receivedContractRequest)
    )
    LOG.info(response.toString())

    // TODO: based on the response of the Lawful Ground Manager send a ContractOffer if Lawful ground manager approves the request or
    // Proxy the received Contract Offer from the Lawful Ground Manager, to the sender of the ContractRequest
    return MultiPart.toResponseEntity(
      MultiPartMessage.Builder()
        .setHeader(response.second.header)
        .setPayload(response.second.payload)
        .build(), HttpStatus.OK
    )
  }

  /**
   * Get the data owners by taking the "id" column of the csv
   */
  private fun getDatasetDataOwners(): List<String> {
    val dataSet = File(_dataProviderConfig.resourcePath).readBytes()
    val targetStream: InputStream = ByteArrayInputStream(dataSet)
    val data = DataFrame.readDelim(targetStream)
    // Take the IDs
    return data[0].toStrings().filterNotNull()
  }
}