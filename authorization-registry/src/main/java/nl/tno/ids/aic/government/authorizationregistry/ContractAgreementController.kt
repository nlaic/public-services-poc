package nl.tno.ids.aic.government.authorizationregistry

import de.fraunhofer.iais.eis.Contract
import de.fraunhofer.iais.eis.ContractAgreement
import de.fraunhofer.iais.eis.ContractOffer
import nl.tno.ids.common.serialization.SerializationHelper
import org.slf4j.LoggerFactory
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("api/contracts")
class ContractAgreementController(private val _contractRepository: ContractRepository) {
    companion object {
        private val LOG = LoggerFactory.getLogger(ContractAgreementController::class.java)
    }
    @CrossOrigin()
    @GetMapping()
    fun getContracts(): List<ContractOffer> {
        LOG.info("Contract get received.")
        return _contractRepository.get()
    }
    @PostMapping
    fun addContract(@RequestBody contractJSON: String) {
        LOG.info("Received contract")
        val contract = SerializationHelper.getInstance().fromJsonLD(contractJSON, ContractOffer::class.java)
        _contractRepository.addcontractAgreement(contract)
    }
}