# public-services-demo
Helm chart for the public services demo, all components necessary for the demo are included in this Helm chart.
A further description is provided below.

## Requirements
- A running kubernetes cluster
- Kubectl configured to use the cluster
- Helm 3

## Deployment content
This umbrella chart deploys the following components using Helm charts:
- IDS broker
- IDS DAPS
- MongoDb instance (used by the broker and DAPS)
- IDS connectors for the Data Providers (CAK, CJIB, Belastingdienst)
- IDS connectors for the Data Consumers (CBS, Gemeent)
- Rule Manager
- Lawlful Ground Manager
- Authorization Registry

>*NOTE:* All component charts are included as local dependencies (`charts/*.tgz`) for simplicity.

### Install command
Make sure to install the Helm chart using
```
helm install public-services .
```