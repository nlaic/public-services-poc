package nl.tno.ids.aic.gov.analysis

import de.fraunhofer.iais.eis.ArtifactRequestMessage
import de.fraunhofer.iais.eis.ArtifactResponseMessageBuilder
import de.fraunhofer.iais.eis.RejectionMessageBuilder
import de.fraunhofer.iais.eis.util.Util
import nl.tno.ids.base.MessageHandler
import nl.tno.ids.common.multipart.MultiPart
import nl.tno.ids.common.multipart.MultiPartMessage
import nl.tno.ids.common.serialization.Config
import nl.tno.ids.common.serialization.DateUtil
import org.slf4j.LoggerFactory
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Component
import java.net.URI

@Component
class AnalysisMessageHandler : MessageHandler<ArtifactRequestMessage> {
  private var _dataAppConfig = Config.dataApp()

  companion object {
    private val LOG = LoggerFactory.getLogger(AnalysisMessageHandler::class.java)
  }

  /**
   * Start a DebtAnalysis once we receive an ArtifaceRequest from municipality
   */
  override fun handle(header: ArtifactRequestMessage, payload: String?): ResponseEntity<*>? {
    LOG.info("Received ArtifactRequestMessage")
    val result = DebtAnalysis().retrieveDataFromProvidersAndAnalyze()
    if (!result.first) {
      // Some contracts where rejected
      return MultiPart.toResponseEntity(MultiPartMessage.Builder()
        .setPayload(result.second)
        .setHeader(RejectionMessageBuilder()
          ._modelVersion_("3.1.0")
          ._issued_(DateUtil.now())
          ._issuerConnector_(URI.create(_dataAppConfig.id))
          ._senderAgent_(URI.create(_dataAppConfig.participant))
          ._recipientAgent_(Util.asList(URI.create(_dataAppConfig.participant)))
          ._recipientConnector_(Util.asList(URI.create(header.issuerConnector.toString())))
          .build())
        .build(), HttpStatus.FORBIDDEN)
    }
    // Contracts accepted
    return MultiPart.toResponseEntity(MultiPartMessage.Builder()
        .setPayload(result.second)
        .setHeader(ArtifactResponseMessageBuilder()
            ._modelVersion_("3.1.0")
            ._issued_(DateUtil.now())
            ._issuerConnector_(URI.create(_dataAppConfig.id))
            ._senderAgent_(URI.create(_dataAppConfig.participant))
            ._recipientAgent_(Util.asList(URI.create(_dataAppConfig.participant)))
            ._recipientConnector_(Util.asList(URI.create(header.issuerConnector.toString())))
            .build())
        .build(), HttpStatus.OK)
  }
}
