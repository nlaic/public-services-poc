package nl.tno.ids.aic.gov.analysis

import org.slf4j.LoggerFactory
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("api")
class AnalysisController {
  companion object {
    private val LOG = LoggerFactory.getLogger(AnalysisController::class.java)
  }

  @GetMapping("analysis")
  fun analysis(): ResponseEntity<String> {
    LOG.info("Received analysis request")
    val debtAnalysis = DebtAnalysis().retrieveDataFromProvidersAndAnalyze()
    if (!debtAnalysis.first) {
      return ResponseEntity.status(HttpStatus.FORBIDDEN).build()
    }
    return ResponseEntity.ok(debtAnalysis.second)
  }
}