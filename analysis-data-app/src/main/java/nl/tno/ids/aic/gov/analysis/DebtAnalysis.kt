package nl.tno.ids.aic.gov.analysis

import com.google.gson.Gson
import de.fraunhofer.iais.eis.*
import de.fraunhofer.iais.eis.util.RdfResource
import de.fraunhofer.iais.eis.util.Util
import krangl.DataFrame
import krangl.readDelim
import nl.tno.ids.base.IDSRestController
import nl.tno.ids.common.multipart.MultiPartMessage
import nl.tno.ids.common.serialization.Config
import nl.tno.ids.common.serialization.DateUtil
import nl.tno.ids.common.serialization.SerializationHelper
import org.slf4j.LoggerFactory
import java.io.ByteArrayInputStream
import java.io.InputStream
import java.net.URI

data class DebtScore(val id: String, val score: Int)
class DebtAnalysis {
  private val _dataAppConfig = Config.dataApp()

  companion object {
    private val LOG = LoggerFactory.getLogger(AnalysisMessageHandler::class.java)
  }

  /**
   * Retrieving the data from providers involves the following steps:
   * 1. Negotiate a contract with the Data Providers (Belastingdienst, CAK and CJIB)
   * 1.1 If any of the contract negoatiations fails, don't do debt analysis.
   * 2. Retrieve the actual resource from each of the Data Provider
   * Then, we perform analysis and return the result as an ArtifactResponse
   * @return boolean whether all contracts were accepted, and the actual debt analysis as json string
   */
  fun retrieveDataFromProvidersAndAnalyze(): Pair<Boolean, String> {
    val belastingDienstData = negotiateContractAndRetrieveData("urn:ids:connectors:Belastingdienst")
    val cakData = negotiateContractAndRetrieveData("urn:ids:connectors:CAK")
    val cjibData = negotiateContractAndRetrieveData("urn:ids:connectors:CJIB")
    if (belastingDienstData == null || cakData == null || cjibData == null) {
      // Some of the ContractRequests was rejected, so can't perform debt analysis.
      return Pair(false, "")
    }
    return Pair(true, analyzeDebts(belastingDienstData, cakData, cjibData))
  }

  private fun analyzeDebts(belastingDienstData: String, cakData: String, cjibData: String): String {
    LOG.info("Initiating Analysis")
    if (belastingDienstData.isNotEmpty()) {
      val targetStream: InputStream = ByteArrayInputStream(belastingDienstData.toByteArray())
      val data = DataFrame.readDelim(targetStream)

      try {
        val debtScores = data.rows.map { row ->
          val debt = row["OPENSTAANDE_SCHULD"] as Int
          DebtScore(row["ID"].toString(), debt)
        }

        LOG.info(debtScores.toString())
        var result = Gson().toJson(debtScores)
        LOG.info("Analysis finished, returning result: $result")
        return result
      } catch (e: Exception) {
        LOG.error("An error occured.", e)
      }
    }

    LOG.info("No analysis Done")
    return "{}"
  }

  private fun negotiateContractAndRetrieveData(dataProvider: String): String? {
    // TODO: Requested Artifact URN
    val requestedArtifact = "data.csv"
    val response = sendContractRequest(requestedArtifact, dataProvider)
    if (response.header is ContractOfferMessage) {
      val contractOffer = SerializationHelper.getInstance().fromJsonLD(response.payload, ContractOffer::class.java)
      return sendArtifactRequest(requestedArtifact, contractOffer.id, dataProvider)
    }
    return null
  }

  /**
   * Send a ArtifactRequest to a DataProvider and receive the artifact's content as String.
   */
  private fun sendArtifactRequest(requestedArtifact: String, contractOfferId: URI, dataProvider: String): String {
    val response = IDSRestController.sendHTTP(
      dataProvider,
      ArtifactRequestMessageBuilder()
        ._modelVersion_("3.1.0")
        ._issued_(DateUtil.now())
        ._issuerConnector_(URI.create(_dataAppConfig.id))
        ._senderAgent_(URI.create(_dataAppConfig.id))
        ._recipientAgent_(Util.asList(URI.create(dataProvider)))
        ._recipientConnector_(Util.asList(URI.create(dataProvider)))
        ._transferContract_(contractOfferId)
        ._requestedArtifact_(URI.create(requestedArtifact))
        .build(), ""
    )
    LOG.info("Receiver ArtifactResponse for dataProvider: $dataProvider")
    return response.second.payload
  }

  /**
   * Send a ContractRequest to a DataProvider and receive a MultiPartMessage as response.
   */
  private fun sendContractRequest(artifact: String, dataProvider: String): MultiPartMessage {
    LOG.info("Sending ContractRequests for dataProvider: $dataProvider")
    val contractRequest = getContractRequest(artifact, dataProvider)
    val response = IDSRestController.sendHTTP(
      dataProvider,
      getContractRequestHeader(dataProvider), SerializationHelper.getInstance().toJsonLD(contractRequest)
    )
    LOG.info("Received ContractOffer for dataProvider: $dataProvider")
    return response.second
  }

  private fun getContractRequestHeader(dataProvider: String): ContractRequestMessage {
    return ContractRequestMessageBuilder()
      ._modelVersion_("3.1.0")
      ._issued_(DateUtil.now())
      ._issuerConnector_(URI.create(_dataAppConfig.id))
      ._senderAgent_(URI.create(_dataAppConfig.id))
      ._recipientConnector_(Util.asList(URI.create(dataProvider)))
      ._recipientAgent_(Util.asList(URI.create(dataProvider)))
      .build()
  }

  /**
   * Build an IDS ContractRequest for a certain artifact and data provider.
   */
  private fun getContractRequest(artifact: String, dataProvider: String): ContractRequest {
    // Permission rule to use a certain artifact under some constraints.
    val dataConsumerPermission: Permission = PermissionBuilder()
      ._assignee_(ArrayList<Participant>(listOf(ParticipantBuilder(URI.create(_dataAppConfig.id)).build())))
      ._targetArtifact_(ArtifactBuilder(URI.create(artifact)).build())
      ._constraint_(
        ArrayList<Constraint>(
          listOf(
            ConstraintBuilder()
              ._leftOperand_(LeftOperand.PURPOSE)
              ._operator_(BinaryOperator.EQ)
              // TODO: URN
              ._rightOperand_(RdfResource("urn:ids:purpose:DEBT_ANALYSIS"))
              .build()
          )
        )
      )
      .build()

    // Minimal contract request with permission to use a certain artifact.
    // Data consumer (CBS) sends this to the data provider (e.g. belastingdienst)
    // Data provider finds the list of data owners associated with this artifact.
    // Data provider then forwards this contract request together with a list of data owners to the
    // Data Provider Lawful Ground Manager.
    return ContractRequestBuilder()
      ._contractStart_(DateUtil.now())
      ._consumer_(URI.create(_dataAppConfig.id))
      ._provider_(URI.create(dataProvider))
      ._permission_(ArrayList<Permission>(listOf(dataConsumerPermission)))
      .build()
  }
}