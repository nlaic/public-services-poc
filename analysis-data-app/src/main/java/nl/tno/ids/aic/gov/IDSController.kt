package nl.tno.ids.aic.gov

import de.fraunhofer.iais.eis.*
import de.fraunhofer.iais.eis.util.Util.asList
import nl.tno.ids.aic.gov.analysis.AnalysisMessageHandler
import nl.tno.ids.base.IDSRestController
import nl.tno.ids.base.ResponseBuilder
import nl.tno.ids.common.multipart.MultiPart
import nl.tno.ids.common.multipart.MultiPartMessage
import nl.tno.ids.common.serialization.Config
import nl.tno.ids.common.serialization.DateUtil
import org.springframework.boot.autoconfigure.EnableAutoConfiguration
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.scheduling.annotation.EnableScheduling
import org.springframework.web.bind.annotation.RestController
import java.net.URI
import javax.annotation.PostConstruct

@EnableScheduling
@RestController
@EnableAutoConfiguration(exclude = [DataSourceAutoConfiguration::class, HibernateJpaAutoConfiguration::class])
open class IDSController : IDSRestController() {

  @PostConstruct
  fun init() {
    LOG.info("Starting Provider message handler")
    registerMessageHandler(AnalysisMessageHandler())
  }

  companion object {
    fun createRejectionEntity(message: Message?, reason: RejectionReason?): ResponseEntity<String>? {
      return MultiPart.toResponseEntity(MultiPartMessage.Builder()
          .setHeader(ResponseBuilder.createRejectionMessage(message, reason).build())
          .build(), HttpStatus.OK)
    }
    fun createMessageProcessedNotificationEntity(message: Message?): ResponseEntity<String>? {
      return MultiPart.toResponseEntity(MultiPartMessage.Builder()
          .setHeader(ResponseBuilder.createMessageProcessedNotification(message).build())
          .build(), HttpStatus.OK)
    }

    fun artifactResponseMessageHeaderBuilder(recipientConnector: String): ArtifactResponseMessage {
      return ArtifactResponseMessageBuilder()
              ._modelVersion_("3.0.0")
              ._issued_(DateUtil.now())
              ._issuerConnector_(URI.create(Config.dataApp().id))
              ._senderAgent_(URI.create(Config.dataApp().participant))
              ._recipientAgent_(asList(URI.create(Config.dataApp().participant)))
              ._recipientConnector_(asList(URI.create(recipientConnector)))
              .build()
    }
  }
}