package nl.tno.ids.aic.government.rulemanager

import org.springframework.stereotype.Component
import java.util.*

data class DataProviderRule(
  val dataConsumer: String,
  val dataProvider: String,
  val allowDebtAnalysis: Boolean,
  val disallowOutsideNl: Boolean,
  val allowedBefore: Date?
)

data class DataOwnerRule(val dataOwnerId: Int, var rulePerProvider: MutableList<DataProviderRule>)

@Component
class DataOwnerRuleRepository {
  private val _dataOwnerRule = mutableListOf<DataOwnerRule>()

  fun get(): List<DataOwnerRule> {
    return _dataOwnerRule
  }

  fun get(id: Int): DataOwnerRule? {
    return _dataOwnerRule.singleOrNull { dc -> dc.dataOwnerId == id }
  }

  fun create(id: Int): DataOwnerRule {
    val dataOwnerRule = DataOwnerRule(id, mutableListOf())
    _dataOwnerRule.add(dataOwnerRule)
    return dataOwnerRule
  }

  fun addProviderRule(id: Int, rule: DataProviderRule) {
    val ownerRule = get(id) ?: create(id)
    ownerRule.rulePerProvider.add(rule)
  }

  fun deleteProviderRule(id: Int, rule: DataProviderRule) {
    val ownerRule = get(id) ?: create(id)
    ownerRule.rulePerProvider.removeIf { it.dataConsumer == rule.dataConsumer && it.dataProvider == rule.dataProvider }
  }

  fun update(updatedDataOwnerRule: DataOwnerRule) {
    _dataOwnerRule.find { dc -> dc.dataOwnerId == updatedDataOwnerRule.dataOwnerId }?.rulePerProvider =
      updatedDataOwnerRule.rulePerProvider
  }
}