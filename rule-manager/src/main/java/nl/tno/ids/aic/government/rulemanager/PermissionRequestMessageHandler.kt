package nl.tno.ids.aic.government.rulemanager

import de.fraunhofer.iais.eis.*
import de.fraunhofer.iais.eis.util.RdfResource
import de.fraunhofer.iais.eis.util.Util
import kotlinx.serialization.*
import kotlinx.serialization.json.*
import nl.tno.ids.base.MessageHandler
import nl.tno.ids.common.multipart.MultiPart
import nl.tno.ids.common.multipart.MultiPartMessage
import nl.tno.ids.common.serialization.DateUtil
import nl.tno.ids.common.serialization.SerializationHelper
import org.slf4j.LoggerFactory
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Component
import java.net.URI

@Serializable
data class PermissionRequest(val dataConsumer: String, val dataProvider: String, val dataOwners: List<Int>)

@Component
class PermissionRequestMessageHandler(
  private val _dataOwnerRuleRepository: DataOwnerRuleRepository,
  private val _legalRuleManager: LegalRuleRepository,
) :
  MessageHandler<RequestMessage> {

  companion object {
    private val LOG = LoggerFactory.getLogger(PermissionRequestMessageHandler::class.java)
  }

  /**
   * Get the Data Consumer, Data Provider and Data Owners from the payload of a Request.
   * Then, for each data owner, build a Permission corresponding to the DataOwner and DataProvider.
   */
  override fun handle(header: RequestMessage?, payload: String?): ResponseEntity<*> {
    val contractRequest: PermissionRequest = Json.decodeFromString(payload!!)
    val permissionsPerOwner = mutableListOf<Permission>()
    for (dataOwnerId in contractRequest.dataOwners) {
      LOG.info("Gathering rules for data owner: ${dataOwnerId}")
      val dataOwnerRule = _dataOwnerRuleRepository.get(dataOwnerId)
      LOG.info("Rules for data owner ${dataOwnerId} gathered")

      LOG.info("Checking for applicable rules for data owner ${dataOwnerId}")
      // Find rule for the consumer / provider combination.
      val dataProviderRule = dataOwnerRule?.rulePerProvider?.find {
        it.dataConsumer == contractRequest.dataConsumer
            &&
            it.dataProvider == contractRequest.dataProvider
      }
      if (dataProviderRule != null) {
        LOG.info("Applicable rules found for data owner ${dataOwnerId}")
        permissionsPerOwner.add(buildPermission(dataOwnerId.toString(), dataProviderRule))
      }
      else {
        LOG.info("Found no applicable rules for data owner ${dataOwnerId}")
      }
    }
    // Add the legal rule as permissions
    for (legalRule in _legalRuleManager.get()) {
      val legalDataProviderRule = legalRule.rulePerProvider.find {
        it.dataConsumer == contractRequest.dataConsumer
                && it.dataProvider == contractRequest.dataProvider
      }
      if (legalDataProviderRule != null) {
        LOG.info("Applicable legal rule found from legal entity: ${legalRule.legalEntity}.")
        permissionsPerOwner.add(buildPermission(legalRule.legalEntity, legalDataProviderRule))
      }
    }

    LOG.info("Finished, returning rules")
    LOG.info(SerializationHelper.getInstance().toJsonLD(permissionsPerOwner))

    return MultiPart.toResponseEntity(
      MultiPartMessage.Builder()
      .setHeader(ContractOfferMessageBuilder()
        ._modelVersion_("3.1.0")
        ._issued_(DateUtil.now())
        ._issuerConnector_(header?.recipientConnector?.get(0))
        ._senderAgent_(header?.recipientAgent?.get(0))
        ._recipientConnector_(Util.asList(header?.issuerConnector))
        ._recipientAgent_(Util.asList(header?.issuerConnector))
        .build())
      .setPayload(SerializationHelper.getInstance().toJsonLD(permissionsPerOwner))
      .build(), HttpStatus.OK)
  }

  private fun buildPermission(assigner: String, rule: DataProviderRule): Permission {
    LOG.info("Building permissions for $assigner.")
    val permissions = PermissionBuilder()
      ._assigner_(ArrayList<Participant>(listOf(ParticipantBuilder(URI.create(assigner)).build())))
      ._assignee_(ArrayList<Participant>(listOf(ParticipantBuilder(URI.create(rule.dataConsumer)).build())))
      ._targetArtifact_(ArtifactBuilder(URI.create("data.csv")).build())
      ._constraint_(
        java.util.ArrayList<Constraint>(
          listOf(
            rule.allowDebtAnalysis.let {
              ConstraintBuilder()
                ._leftOperand_(LeftOperand.PURPOSE)
                ._operator_(BinaryOperator.EQ)
                ._rightOperand_(RdfResource("urn:ids:purpose:DEBT_ANALYSIS"))
                .build()
            },
            rule.disallowOutsideNl.let {
              ConstraintBuilder()
                ._leftOperand_(LeftOperand.ABSOLUTE_SPATIAL_POSITION)
                ._operator_(BinaryOperator.IN)
                ._rightOperand_(RdfResource("(http://dbpedia.org/resource/Netherlands)"))
                .build()
            },
            rule.allowedBefore?.let { allowedBefore ->
              ConstraintBuilder()
                ._leftOperand_(LeftOperand.POLICY_EVALUATION_TIME)
                ._operator_(BinaryOperator.BEFORE)
                ._rightOperand_(
                  RdfResource(DateUtil.asXMLGregorianCalendar(allowedBefore).toXMLFormat())
                )
                .build()
            }
          )
        )
      )
      .build()
    LOG.info("Finished building rules for ${assigner}")
    return permissions
  }
}