package nl.tno.ids.aic.government.lawfulgroundmanager

import nl.tno.ids.aic.government.ContractOfferRepository
import org.slf4j.LoggerFactory
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("api/lawful-ground-manager")
class LawfulGroundManagerController(private val _contractOfferRepository: ContractOfferRepository) {
    companion object {
        private val LOG = LoggerFactory.getLogger(LawfulGroundManagerController::class.java)
    }

    @PostMapping("{id}")
    fun dataOwnerRuleUpdated(@PathVariable id: String) {
        LOG.info("Received updated rule from Rule Manager")
        _contractOfferRepository.deleteContractsWithDataOwner(id)
    }
}