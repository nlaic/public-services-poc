package nl.tno.ids.aic.government

class LawfulGroundManagerConfig {
  val ruleManagers: List<String> = listOf("")
  val authorizationRegistry: String = ""
}