# Analysis data app
This repository contains a Kotlin application that acts as the Lawful Ground Manager in the public services PoC ecosystem.

>*NOTE:* This project is part of a Proof of Concept and therefore does not contain production ready code!

## Building the project
Gradle is used as build tool, build the project by running ``./gradlew build``.

## Configuration
The Data App needs some YAML configuration values placed in ``/ids/config.yaml`` to work properly:

| Key                                    | Description                             |
| -------------------------------------- | --------------------------------------- |
| id                                     | URN of the Lawful Ground Manager        |
| participant                            | URN of the participant                  |
| customProperties.authorizationRegistry | Endpoint for the authorization registry |
| customProperties.ruleManagers          | Collection of IDS IDs rule managers     |
